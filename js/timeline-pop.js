var g = {};

g.groups = [
    {
        id: 1,
        content: "Matière principale"
    },
    {
        id: 2,
        content: "Projets",
        className: "red"
    },
/*    {
        id: 3,
        content: "Matière secondaire",
        className: "green"
    }*/
];

g.events = [
    {
        content: "Bases SQL",
        title: "Connaître PHPMyAdmin, savoir écrire à la main les 4 requêtes de bases (SELECT/INSERT/UPDATE/DELETE).<br><a href='https://dev.mysql.com/doc/'>Doc MySQL</a>",
        start: "2017-08-16",
        group: 1
    },
    {
        content: "SQL et PHP",
        title: "À la fin de cette semaine, vous savez utiliser des bases de données. Vous savez ce qu’est une jointure et comment réfléchir puis concevoir une base de données. Nous utilisons PHP pour la mise en application.<br><a href='http://php.net/manual/en/mysql.php'>PHP DOC sur MySQL</a>",
        start: "2017-08-22",
        group: 1
    },
    {
        content: "JSON et VanillaJS",
        title: "Nous abordons le JSON et son usage en vanilla JS. À la fin de la semaine, vous savez créer des données en JSON, fusse via un outil externe, et les exploiter en JavaScript. Cette semane est aussi une révision de VanillaJS …",
        start: "2017-08-29",
        group: 1
    },
    {
        content: "XHR et VanillaJS",
        title: "Nous allons lancer des requêtes sur des APIs en VanillaJS. Le but est d’envoyer ou de récupérer des données en JSON",
        start: "2017-09-05",
        group: 1
    },
    {
        content: "VueJS",
        title: "Premiers pas avec un Framework JavaScript très léger. Histoire de se familiariser avec un Framework. Les copier/coller d’anciens code seront possibles avec Vue (ils ne le seront plus en Angular ou Ionic)",
        start: "2017-09-12",
        group: 1
    },
    {
        content: "Cordova",
        title: "Nous allons explorer Cordova. En se basant sur des projets existants, nous allons ajouter des fonctionnalités propres aux smartphones. Le copier/coller sera possible, on pourra travailler en VanillaJS comme en Vue",
        start: "2017-09-19",
        group: 1
    },
    {
        content: "Angular",
        title: "Deux options: Angular (alias Angular2) pour ceux qui s’en sentent capables, AngularJS (alias Angular1) pour ceux qui préfèrent la jouer safe. Les copiers/collers ne sont plus possibles en Angular, et sont compliqués en AngularJS …",
        start: "2017-09-26",
        group: 1
    },
    {
        content: "Ionic",
        title: "Ionic2 ou Ionic1, en fonction de ce que vous avez vu avec Angular ou AngularJS, évidement",
        start: "2017-10-10",
        group: 1
    },
    {
        content: "Reprise",
        title: "À ce stade, les wireframes doivent être terminées et en cours de validation avec le client",
        start: "2017-08-18",
        group: 2
    },
    {
        content: "Run1",
        start: "2017-08-25",
        group: 2
    },
    {
        content: "Run2",
        start: "2017-09-08",
        group: 2
    },
    {
        content: "Run3",
        title: "Première RC",
        start: "2017-09-22",
        group: 2
    },
    {
        content: "Run4",
        title: "Ajustements clients",
        start: "2017-10-06",
        group: 2
    },
    {
        content: "Livraison",
        title: "Là, vous devriez avoir rendu le projet",
        start: "2017-10-21",
        group: 2
    }
];

document.addEventListener("DOMContentLoaded", function() {
    var container = document.getElementById("timeline");
    var events = new vis.DataSet(g.events);
    var options = {};

    var timeline = new vis.Timeline(container, events, g.groups, options);
});
